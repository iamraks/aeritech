﻿using Aeritch.DataAccess.Core.DatabaseContext;
using Aeritch.DataAccess.Core.Repository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Aeritch.Infrastructure.Repository
{
	public class Repository<T> : IAsyncRepository<T> where T : class
	{
		protected readonly DataContext _context;

		public Repository(DataContext dbContext)
		{
			this._context = dbContext;
		}

		public virtual async Task<IEnumerable<T>> GetAllAsyn()
		{

			return await _context.Set<T>().ToListAsync();
		}


		public virtual async Task<T> GetAsync(int id)
		{
			return await _context.Set<T>().FindAsync(id);
		}

		public virtual async Task<IEnumerable<T>> GetWhere(Expression<Func<T, bool>> predicate)
		{
			return await _context.Set<T>().Where(predicate).ToListAsync();
		}

	

		private bool disposed = false;
		protected virtual void Dispose(bool disposing)
		{
			if (!this.disposed)
			{
				if (disposing)
				{
					_context.Dispose();
				}
				this.disposed = true;
			}
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}
	}
}
