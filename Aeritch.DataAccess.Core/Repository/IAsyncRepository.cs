﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Aeritch.DataAccess.Core.Repository
{
	public interface IAsyncRepository<T> where T : class
	{
		Task<IEnumerable<T>> GetAllAsyn();
		Task<T> GetAsync(int id);
		Task<IEnumerable<T>> GetWhere(Expression<Func<T, bool>> predicate);
	}
}
