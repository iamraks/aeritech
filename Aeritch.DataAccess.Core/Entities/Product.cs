﻿using Aeritch.DataAccess.Core.Entities;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Aeritech.DataAccess.Core.Entities
{
	[Table("Product")]
	public class Product
	{
		[Key]
		public int ProductID { get; set; }
		public string Name { get; set; }
		public string ProductNumber { get; set; }
		public decimal ListPrice { get; set; }
		public string Color { get; set; }
		public string Size { get; set; }

	}

}

