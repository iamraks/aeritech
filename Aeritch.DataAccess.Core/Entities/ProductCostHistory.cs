﻿using Aeritech.DataAccess.Core.Entities;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Aeritch.DataAccess.Core.Entities
{
	public class ProductCostHistory
	{
		public int ProductID { get; set; }
		public DateTime StartDate { get; set; }
		public DateTime? EndDate { get; set; }
		public decimal StandardCost { get; set; }
	}
}
