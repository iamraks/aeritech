﻿using Aeritch.DataAccess.Core.Entities;
using Aeritech.DataAccess.Core.Entities;
using Microsoft.EntityFrameworkCore;

namespace Aeritch.DataAccess.Core.DatabaseContext
{
	public class DataContext : DbContext
	{
		public DataContext(DbContextOptions<DataContext> options)
					: base(options)
		{
		}
		public DbSet<Product> Products { get; set; }
		public DbSet<ProductCostHistory> ProductCostHistory { get; set; }

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			//Configure default schema
			modelBuilder.HasDefaultSchema("Production");
			modelBuilder.Entity<ProductCostHistory>().HasKey(e => new { e.ProductID, e.StartDate });
		}
	}
}
