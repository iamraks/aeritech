﻿using Aeritech.Core.Services;
using Aeritech.Infrastructure.Services;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Aeritech.Web.Controller
{
	[ApiController]
	public class ProductsController : ControllerBase
	{
		private readonly IProductProvider productProvider;
		private readonly IProductCostHistoryProvider productCostHistoryProvider;

		public ProductsController(IProductProvider productProvider, IProductCostHistoryProvider productCostHistoryProvider)
		{
			this.productProvider = productProvider;
			this.productCostHistoryProvider = productCostHistoryProvider;

		}

		[Route("api/v1/products", Name = "GetProducts")]
		[HttpGet]
		public async Task<IActionResult> Get()
		{
			var product = await this.productProvider.Get();
			if (product == null)
				return NotFound("No record found.");

			return Ok(product);
		}

		[Route("api/v1/products/{id}", Name = "GetProducts details")]
		[HttpGet]
		public async Task<IActionResult> GetDetails(int id)
		{
			var product = await this.productProvider.GetById(id);
			if (product == null)
				return NotFound("No record found.");

			return Ok(product);
		}

		[Route("api/v1/productscosthistory/{id}", Name = "Get Products Cost History details")]
		[HttpGet]
		public async Task<IActionResult> GetProdoductCostHistoryDetails(int id)
		{
			var product = await this.productCostHistoryProvider.Get(id);
			if (product == null)
				return NotFound("No record found.");

			return Ok(product);
		}
	}
}
