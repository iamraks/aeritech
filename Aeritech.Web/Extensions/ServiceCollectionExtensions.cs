﻿using Aeritch.DataAccess.Core.DatabaseContext;
using Aeritch.DataAccess.Core.Repository;
using Aeritch.Infrastructure.ModelMap;
using Aeritch.Infrastructure.Repository;
using Aeritech.Core.Providers;
using Aeritech.Core.Services;
using Aeritech.Infrastructure.ModelMap;
using Aeritech.Infrastructure.Providers;
using Aeritech.Infrastructure.Services;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Aeritech.Web.Extensions
{
	internal static class ServiceCollectionExtensions
	{
		public static IServiceCollection AddDataContexts(this IServiceCollection services, IConfiguration configuration)
		{
			services.AddDbContext<DataContext>(options => { options.UseSqlServer(configuration.GetConnectionString("DefaultConnection")); });
			return services;
		}
		public static IServiceCollection AddAutoMapperAssemblies(this IServiceCollection services)
		{
			var mappingConfig = new MapperConfiguration(mc =>
			{
				mc.AddProfile(new ProductModelMap());
				mc.AddProfile(new ProductCostHistoryMap());

			});
			IMapper mapper = mappingConfig.CreateMapper();
			services.AddSingleton(mapper);
			return services;
		}
		public static IServiceCollection AddRepositories(this IServiceCollection services)
		{
			services.AddScoped(typeof(IAsyncRepository<>), typeof(Repository<>));
			services.AddScoped(typeof(IProductProvider), typeof(ProductProvider));
			services.AddScoped(typeof(IProductCostHistoryProvider), typeof(ProductCostHistoryProvider));


			return services;
		}
	}
}
