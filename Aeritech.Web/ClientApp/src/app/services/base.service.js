"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var BaseService = /** @class */ (function () {
    function BaseService() {
        this.baseApiUrl = document.location.origin + 'api/v1/';
    }
    return BaseService;
}());
exports.BaseService = BaseService;
//# sourceMappingURL=base.service.js.map