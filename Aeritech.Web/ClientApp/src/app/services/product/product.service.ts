import { BaseService } from "../base.service";
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

@Injectable()
export class ProductService extends BaseService {
  constructor(
    private http: HttpClient) {
    super();
  }

  getProduct() {
    const url = this.baseApiUrl + "products";
    return this.http.get<any>(url);
  }

  getProductDetail(productID: number) {
    const url = this.baseApiUrl + "products/" + productID;
    return this.http.get<any>(url);
  }

  getProductCostHistoryDetail(productID: number) {
    const url = this.baseApiUrl + "productscosthistory/" + productID;
    return this.http.get<any>(url);
  }
}
