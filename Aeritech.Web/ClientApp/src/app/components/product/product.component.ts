import { OnInit, Component } from "@angular/core";
import { ProductService } from "../../services/product/product.service";
import { State, process } from "@progress/kendo-data-query";
import { DataStateChangeEvent, PageSizeItem } from "@progress/kendo-angular-grid";

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html'
})
export class ProductComponent implements OnInit {
  public gridData: any;
  isDetail = false;
  isDataLoaded = false;
  public state: State = {
    skip: 0,
    take: 15
  };
  
  public buttonCount = 5;
  public info = true;
  public type: 'numeric' | 'input' = 'numeric';
  pageSizes: PageSizeItem[] = [
    {
    text: 'All',
    value: 'all'
    },
    {
      text: '15',
      value: 15
    },
    {
      text: '50',
      value: 50
    },
    {
      text: '150',
      value: 150
    }
  ];
  public previousNext = true;

  public pageSize = 15;
  public skip = 0;

  constructor(private productService: ProductService) { }

  ngOnInit() {
    this.getProduct();
  }
  getProduct() {
    this.productService.getProduct().subscribe((data) => {
      this.gridData = process(data, this.state);
      this.isDataLoaded = true;

    });
  }

  public dataStateChange(state: DataStateChangeEvent): void {
    this.state = state;
    this.getProduct();
  }
}

