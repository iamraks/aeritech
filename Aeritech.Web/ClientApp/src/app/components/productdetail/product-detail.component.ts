import { OnInit, Component, AfterViewInit } from "@angular/core";
import { ProductService } from "../../services/product/product.service";
import { State } from "@progress/kendo-data-query";
import { DataStateChangeEvent, PageSizeItem } from "@progress/kendo-angular-grid";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html'
})
export class ProductDetailComponent implements OnInit {
  public gridDetailData: any;
  public gridCostHistory: any;
  productID: number;
  errorMessage: any;
  isCostHistoryAvailable = false;
  isError = false;
  public state: State = {
    skip: 0,
    take: 20
  };
  isDetail = true;
  public pageSizes: PageSizeItem[] = [
    {
      text: 'All',
      value: 'all'
    },
    {
      text: '20',
      value: 20
    }
  ];


  constructor(private productService: ProductService, private route: ActivatedRoute) { }
  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.productID = params.id;
      this.productDetail(this.productID);
    });
  }

  public dataStateChange(state: DataStateChangeEvent): void {
    this.state = state;
    this.productDetail(this.productID);
  }

  public productDetail(productID) {
    this.productService.getProductDetail(productID).subscribe((data) => {
      this.gridDetailData = data;
      this.productCostHistoryDetail(productID);
    },
      error => {
        this.isError = true;
        this.errorMessage = error.error;
      }
    );
  }
 

  public productCostHistoryDetail(productID) {
    this.productService.getProductCostHistoryDetail(productID).subscribe((data) => {
      if (data !== null) {
        this.isCostHistoryAvailable = data.length > 0 ? true : false;
        let i: number;
        for (i = 0; i < data.length; i++) {
          data[i].startDate = new Date(data[i].startDate)
          data[i].endDate = data[i].endDate===null?"": new Date(data[i].endDate)
        }
      }
      this.gridCostHistory = data;
    },
      error => {
        this.isError = true;
        this.errorMessage = error.error; 
      }
    );
  }
}

