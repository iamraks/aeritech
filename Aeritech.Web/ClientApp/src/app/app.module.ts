import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';

import { ProductComponent } from './components/product/product.component';
import { ProductService } from './services/product/product.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { GridModule, ExcelModule   } from '@progress/kendo-angular-grid';
import { ProductDetailComponent } from './components/productdetail/product-detail.component';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    ProductComponent,
    ProductDetailComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    GridModule, ExcelModule ,
    RouterModule.forRoot([
      { path: '', component: ProductComponent, pathMatch: 'full' },
      { path: 'detail/:id', component: ProductDetailComponent }

    ])
  ],
  exports: [RouterModule],
  providers: [
    { provide: LOCALE_ID, useValue: "en-US" },
    { provide: 'BASE_URL', useFactory: getBaseUrl },
    ProductService
            ],
  bootstrap: [AppComponent]
})
export class AppModule { }

export function getBaseUrl() {
  return document.getElementsByTagName('base')[0].href;
}
