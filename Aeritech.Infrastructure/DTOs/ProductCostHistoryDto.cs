﻿using System;

namespace Aeritech.Infrastructure.DTOs
{
	public class ProductCostHistoryDto
	{
		public DateTime StartDate { get; set; }
		public DateTime? EndDate { get; set; }
		public decimal StandardCost { get; set; }

	}
}
