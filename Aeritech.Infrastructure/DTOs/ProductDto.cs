﻿using Aeritech.Infrastructure.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace Aeritech.Core.DTOs
{
	public class ProductDto
	{
		public int ProductID { get; set; }
		public string Name { get; set; }
		public string ProductNumber { get; set; }
		public decimal ListPrice { get; set; }
		public string Color { get; set; }
		public string Size { get; set; }

	}
}
