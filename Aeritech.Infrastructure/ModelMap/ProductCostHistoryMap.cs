﻿using Aeritch.DataAccess.Core.Entities;
using Aeritech.Infrastructure.DTOs;
using AutoMapper;

namespace Aeritech.Infrastructure.ModelMap
{
	public class ProductCostHistoryMap : Profile
	{
		public ProductCostHistoryMap()
		{
			this.CreateMap<ProductCostHistory, ProductCostHistoryDto>();

		}
	}
}
