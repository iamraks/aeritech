﻿using Aeritech.Core.DTOs;
using Aeritech.DataAccess.Core.Entities;
using AutoMapper;
namespace Aeritch.Infrastructure.ModelMap
{
	public class ProductModelMap : Profile
	{
		public ProductModelMap()
		{
			this.CreateMap<Product, ProductDto>();

		}
	}

}
