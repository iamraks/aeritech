﻿using Aeritech.Core.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Aeritech.Core.Services
{
	public interface IProductProvider
	{
		Task<IEnumerable<ProductDto>> Get();
		Task<ProductDto> GetById(int id);
	}
}
