﻿using Aeritech.Infrastructure.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Aeritech.Infrastructure.Services
{
	public interface IProductCostHistoryProvider
	{
		Task<IEnumerable<ProductCostHistoryDto>> Get(int productID);

	}
}
