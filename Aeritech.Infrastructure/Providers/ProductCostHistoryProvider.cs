﻿using Aeritch.DataAccess.Core.Entities;
using Aeritch.DataAccess.Core.Repository;
using Aeritech.Infrastructure.DTOs;
using Aeritech.Infrastructure.Services;
using AutoMapper;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Aeritech.Infrastructure.Providers
{
	public class ProductCostHistoryProvider : IProductCostHistoryProvider
	{
		private readonly IAsyncRepository<ProductCostHistory> asyncRepository;
		private IMapper mapper;

		public ProductCostHistoryProvider(IAsyncRepository<ProductCostHistory> asyncRepository, IMapper mapper)
		{
			this.asyncRepository = asyncRepository;
			this.mapper = mapper;
		}

		public async Task<IEnumerable<ProductCostHistoryDto>> Get(int productID)
		{
			var product = await asyncRepository.GetWhere(x => x.ProductID == productID);
			return mapper.Map<IEnumerable<ProductCostHistoryDto>>(product);
		}
	}
}
