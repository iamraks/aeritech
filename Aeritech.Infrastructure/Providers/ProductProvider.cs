﻿using Aeritch.DataAccess.Core.Repository;
using Aeritech.Core.DTOs;
using Aeritech.Core.Services;
using Aeritech.DataAccess.Core.Entities;
using AutoMapper;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Aeritech.Core.Providers
{
	public class ProductProvider : IProductProvider
	{
		private readonly IAsyncRepository<Product> asyncRepository;
		private IMapper mapper;

		public ProductProvider(IAsyncRepository<Product> asyncRepository, IMapper mapper)
		{
			this.asyncRepository = asyncRepository;
			this.mapper = mapper;
		}

		public async Task<IEnumerable<ProductDto>> Get()
		{
			var product = await asyncRepository.GetAllAsyn();
			return mapper.Map<IEnumerable<ProductDto>>(product);
		}

		public async Task<ProductDto> GetById(int id)
		{
			var product = await asyncRepository.GetAsync(id);
			return mapper.Map<ProductDto>(product); 
		}
	}
}
